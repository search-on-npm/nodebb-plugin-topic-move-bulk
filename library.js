"use strict";

var controllers = require('./lib/controllers'),
	Categories = require.main.require('./src/categories'),
	Topics = require.main.require('./src/topics'),
	SocketPlugins = require.main.require('./src/socket.io/plugins'),
	async = require.main.require('async'),

	plugin = {};

SocketPlugins.connecttopicmovebulk = {};

plugin.init = function(params, callback) {

	console.log('nodebb-plugin-connect-topic-move-bulk: loaded');

	var router = params.router,
		hostMiddleware = params.middleware,
		hostControllers = params.controllers;

	// We create two routes for every view. One API call, and the actual route itself.
	// Just add the buildHeader middleware to your route and NodeBB will take care of everything for you.

	router.get('/admin/plugins/connect-topic-move-bulk', hostMiddleware.admin.buildHeader, controllers.renderAdminPage);
	router.get('/api/admin/plugins/connect-topic-move-bulk', controllers.renderAdminPage);

	callback();
};

plugin.addAdminNavigation = function(header, callback) {
	header.plugins.push({
		route: '/plugins/connect-topic-move-bulk',
		icon: 'fa-tint',
		name: 'Topic move bulk (Connect)'
	});

	callback(null, header);
};


SocketPlugins.connecttopicmovebulk.search = function(socket, data, callback) {
	var topics_find = {};
	var tot = 0;
	try {
		var title_regexp = new RegExp(data.title, "i");
	} catch (e) {
		return callback({
			message: "Invalid regular expression: change it and try again"
		});
	}
	if (data.categories && Array.isArray(data.categories) && data.categories.length) {
		async.eachSeries(data.categories, function(category, cb) {
			//async.eachLimit(data.tids, 10, function(tid, next) { ????????
			Categories.getCategoryData(category, function(err, categoryData) {
				topics_find[category] = {
					category_name: categoryData.name,
					topics: []
				}
				Categories.getTopicIds('cid:' + category + ':tids', true, 0, -1, function(err, tids) {
					Topics.getTopicsData(tids, function(err, topics) {
						for (var it in topics) {
							if (title_regexp.test(topics[it].title)) {
								topics_find[category]['topics'].push({
									tid: topics[it].tid,
									topic_title: topics[it].title
								});
								tot++;
							}
						}
						cb(null);
					});
				});
			})
		}, function(err, result) {
			return callback(null, {
				tot: tot,
				result: topics_find
			});
		});
	} else {
		return callback(null, {
			tot: 0,
			result: null
		});
	}
};

SocketPlugins.connecttopicmovebulk.move = function(socket, data, callback) {
	//if bla bla
	data.tids = data.tids.split(",");
	if (!data || !Array.isArray(data.tids) || !data.cid) {
		return callback(new Error('[[error:invalid-data]]'));
	}
	var cid = data.cid;
	var uid = socket.uid;
	async.eachLimit(data.tids, 100, function(tid, next) {
		Topics.tools.move(tid, cid, uid, next);
	}, function(err, result) {
		if (err) {
			callback(err, null);
		}
		return callback(null, {
			tot: data.tids.length
		});
	});
};

module.exports = plugin;
