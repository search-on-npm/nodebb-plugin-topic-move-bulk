# Topic-move-bulk Plugin for Connect

**[FUNZIONALITA']**<br />

Permette di spostare topic da una categoria ad un'altra. Per poter effettuare questo spostamento bisogna inserire nel campo Title un'espressione regolare (javascript) che permetta di prendere tutti i titoli che soddisfano tale espressione e spostarli in un'altra categoria. Esempio:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-topic-move-bulk/raw/demaoui/screenshot/connect-topic-bulk-admin.png)
<br />

**[TEST]**<br />

Per testare il plugin basta inserire nel campo Title un'espressione regolare e selezionare il gruppo in cui sono stati pubblicati i topic che si vogliono spostare:<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-topic-move-bulk/raw/demaoui/screenshot/connect-topic-move-bulk-admin2.png) <br />

Come risultato si avrà una lista di topic, che in quella categoria, matchano con l'espressione regolare fornita in input. Una volta che saranno elencati tutti i topic risultato, sarà possibile spostare i topic in un'altra categoria<br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-topic-move-bulk/raw/demaoui/screenshot/connect-topic-move-bulk-admin3.png) <br />

Basterà andare nella categoria in cui si è scelto di spostare i topic e si potrà vedere che effettivamente i topic sono stati spostati.








