define('admin/plugins/connect-topic-move-bulk', ['settings'], function(Settings) {
	'use strict';
	/* globals $, app, socket, require */

	var ACP = {};

	ACP.init = function() {
		$('#search').on('click', function() {
			var title = $('#src_title').val();
			if (title != null && title != "" && title != undefined) {
				var ids = [];
				$("#src_results").hide();
				$("#src_move_results").hide();
				$("#src_loading").show();
				socket.emit('plugins.connecttopicmovebulk.search', {
					title: $("#src_title").val(),
					categories: $("#src_categories").val()
				}, function(err, data) {
					if (err) {
						app.alertError(err.message);
					} else {
						$("#src_results_tot").html('Total topics found: ' + data.tot);
						$("#src_results_list").html('');
						if (data.result && data.result != null) {
							for (var ic in data.result) {
								$("#src_results_list").append('<h4>' + data.result[ic].category_name + ' (' + data.result[ic].topics.length + ')</h4>');
								if (data.result[ic].topics && data.result[ic].topics.length > 0) {
									//$("#src_results_list").append('<ul>');
									var topics_html = '';
									for (var it in data.result[ic].topics) {
										//$("#src_results_list").append('<li>'+data.result[ic].topics[it].topic_title+'</li>');
										topics_html += '<li>' + data.result[ic].topics[it].topic_title + '</li>';
										ids.push(data.result[ic].topics[it].tid);
									}
									$("#src_results_list").append('<ul>' + topics_html + '</ul>');
								}
							}
						} else {
							$("#src_results_list").append('<div>No Topics found</div>');
						}
						/*var ids = data.result.map(function(topic, index) {
						    return topic.tid; 
						});*/
						$("#ids_to_move").val(ids);
						$("#src_results").show();
						$("#src_loading").hide();
					}
				});
			} else {
				app.alert({
					type: 'danger',
					alert_id: 'connect-topic-move-bulk',
					title: 'Wrong Input',
					message: 'Put a value into title field',
				});
			}
		});


		$('#move').on('click', function() {
			$("#src_results").hide();
			$("#src_move_results").hide();
			$("#src_loading").show();
			socket.emit('plugins.connecttopicmovebulk.move', {
				tids: $("#ids_to_move").val(),
				cid: $("#category_target").val()
			}, function(err, data) {
				$('#src_move_results').html('<div class="alert alert-success">' + data.tot + ' topics moved successfully</div>');
				$("#src_loading").hide();
				$("#src_move_results").show();
			});
		});
	};

	return ACP;
});
