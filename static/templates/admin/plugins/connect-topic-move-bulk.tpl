<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<div class="panel-heading">GT Topic move bulk</div>
			<div class="panel-body">
				<p>
					Permette, tramite espressioni regolari, di spostare i topic con un certo titolo da una categoria all'altra. Per testare l'espressioni regolari si può andare ( <a href="https://regex101.com/#javascript">online test</a>)<br />
					Per esempio considerata la parola "prova", di seguito sono riportate le più comuni espressioni regolari:
					<ul>
						<li>Match esato o contiene la parola "prova"  ----><em>prova</em></li>
						<li>Inizia con "prova" ----><em>^prova</em></li>
						<li>Finisce con "prova" ----><em>prova$</em></li>
						<li>Contiene almeno un carattere prima di "prova" ----><em>.+prova</em></li>
						
						
					</ul>
				</p>
				<div class="form-group">
					<label for="Setting 1">Title</label>
					<div class="input-group">					  
					    <input type="text" id="src_title" name="src_title" title="Title" class="form-control" placeholder="Insert a regexp">			   
					    
					</div>
				</div>
				<div class="form-group">
					<label for="Setting 2">Categories</label>
                        <select multiple id="src_categories" name="src_categories">
                        	<!-- BEGIN categories -->
                            <option value="{categories.cid}">{categories.name}</option>
                            <!-- END categories -->
                        </select>
				</div>
				<div>
					<button class="btn btn-primary" id="search">Search</button>
				</div>
				
				<hr />

				<h3>Results:</h3>
				<div id="src_loading" style="display:none">...................</div>
				<div id="src_results" style="display:none">
					<div id="src_results_tot"></div>
					<div id="src_results_action">
						<select id="category_target" name="category_target">
                        	<!-- BEGIN categories -->
                            <option value="{categories.cid}">{categories.name}</option>
                            <!-- END categories -->
                        </select>
                        <input id="ids_to_move" type="hidden" value="" />
						<button class="btn btn-primary" id="move">Move</button>
					</div>
					<div id="src_results_list"></div>
				</div>
				<div id="src_move_results" style="display:none"></div>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">Control Panel</div>
			<div class="panel-body">
				<button class="btn btn-primary" id="save">Save Settings</button>
			</div>
		</div>
	</div>
</div>