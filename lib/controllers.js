'use strict';

var Categories = require.main.require('./src/categories');

var Controllers = {};

Controllers.renderAdminPage = function (req, res, next) {
	/*
		Make sure the route matches your path to template exactly.

		If your route was:
			myforum.com/some/complex/route/
		your template should be:
			templates/some/complex/route.tpl
		and you would render it like so:
			res.render('some/complex/route');
	*/

	Categories.getAllCategories(0, function (err, categoryData) {
		if (err) {
			//TO DO
			//return next(err);
			
		}

		res.render('admin/plugins/connect-topic-move-bulk', {categories:categoryData});
	});
};

module.exports = Controllers;